import 'whatwg-fetch'

export const REQ_TOKEN = 'REQ_TOKEN'
export const STORED_TOKEN = 'STORED_TOKEN'
export const RECEIVE_TOKEN = 'RECEIVE_TOKEN'

export const REQ_COUNTRIES = 'REQ_COUNTRIES'
export const RECEIVE_COUNTRIES = 'RECEIVE_COUNTRIES'
export const RECEIVE_CITIES = 'RECEIVE_CITIES'
export const RECEIVE_ALL_COUNTRIES = 'RECEIVE_ALL_COUNTRIES'

export const INIT_TRIP_FROM_HP = 'INIT_TRIP_FROM_HP'
export const RECEIVE_TRIP_FROM_HP = 'RECEIVE_TRIP_FROM_HP'
export const ADD_TRIP_CITIES = "ADD_TRIP_CITIES"
export const ADD_TRIP_DATES = "ADD_TRIP_DATES"
export const ADD_TRIP_TRANSPORT_WITH_ACCOMODATION = "ADD_TRIP_TRANSPORT_WITH_ACCOMODATION"

export const REQ_BOARD = 'REQ_BOARD'
export const RECEIVE_BOARD = 'RECEIVE_BOARD'

export const ADD_CITIES = "ADD_CITIES"
export const CITIES_ADDED = "CITIES_ADDED"

export const ADD_CITY = "ADD_CITY"
export const CITY_ADDED = "CITY_ADDED"

export const ADD_CARD = "ADD_CARD"






//universal
export function saveToStorage (token) {
    localStorage.setItem('auth_token', token)
}
export function getFromStorage (name, data) {
  return data = localStorage.getItem(name)
}


//actions

//user

  export function addMoreCard(data) {
    return {
      type: ADD_CARD,
      data,
    }
  }
  export function reqToken () {
    return {
      type: REQ_TOKEN,
    }
  }
  export function receiveToken (data) {
    return {
      type: RECEIVE_TOKEN,
      data,
      token: data.token
    }
  }
  export function getTokenFromStorage (token) {
    return{
      type: STORED_TOKEN,
      token
    }
  }

//cities
  export function reqCountries () {
    return {
      type: REQ_COUNTRIES,
    }
  }
  export function receiveCountries (data) {
    return {
      type: RECEIVE_COUNTRIES,
      data,

    }
  }
  export function receiveAllCountries (data) {
    return {
      type: RECEIVE_ALL_COUNTRIES,
      allData: data

    }
  }
  export function receiveCities (startCity, otherCities) {
    return {
      type: RECEIVE_CITIES,
      startCity,
      otherCities
    }
  }

//planboard actions
  export function reqPlanBoard () {
    return {
      type: REQ_BOARD,
    }
  }

  export function receivePlanBoard (data) {
    return {
      type: RECEIVE_BOARD,
      data: data.planningBoardVO.planningBoardElementVOList
    }
  }

  export function addCitiesListReq () {
    return {
      type: ADD_CITIES,
    }
  }

  export function addCitiesListRes (data) {
    return {
      type: CITIES_ADDED,
      data: data.planningBoardVO.planningBoardElementVOList
    }
  }

  export function addCityReq () {
    return {
      type: ADD_CITY,
    }
  }

  export function addCityRes (data) {
    return {
      type: CITY_ADDED,
      data: data.planningBoardVO.planningBoardElementVOList
    }
  }

  export function addTripCities (citiesData) {
    return {
      type: ADD_TRIP_CITIES,
      cities: citiesData
    }
  }

  export function addTripDates (datesData) {
    return {
      type: ADD_TRIP_DATES,
      dates: datesData
    }
  }

  export function addTripTransportWithAcc (transportWithAccData) {
    return {
      type: ADD_TRIP_TRANSPORT_WITH_ACCOMODATION,
      transportWithAcc: transportWithAccData
    }
  }

export function initTripReq() {
  return{
    type: INIT_TRIP_FROM_HP
  }
}

export function initTripRes(data) {
  return{
    type: RECEIVE_TRIP_FROM_HP,
    data
  }
}



//async actions
export function logIn () {
  return dispatch => {
    dispatch(reqToken())
    if (localStorage.auth_token) {
      const token = localStorage.auth_token
      return dispatch(getTokenFromStorage(token))
    } else {
      return fetch(`http://tripcomposer.net:12345/init/user`)
                .then(response => response.json())
                .then(data => dispatch(receiveToken(data)))
                .then(token => saveToStorage(token.token))
    }
  }
}

export function citiesToCountries (city) {
  return dispatch => {
    dispatch(reqCountries())
    return fetch(`http://tripcomposer.net:12345/find/city?city=${city}`)
              .then(response => response.json())
              .then(data => dispatch(receiveCountries(data)))
  }
}

export function initPlanBoard (token) {
  return dispatch => {
    dispatch(reqPlanBoard())

    const authToken = `Bearer ${token}`
    const url = `http://tripcomposer.net:12345/rest/planning-board`;
    const myInit = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        'authorization': authToken,
      }
    }
    return fetch(url, myInit)
              .then(response => response.json())
              .then(data => dispatch(receivePlanBoard(data)))
  }
}

export function resetPlanBoard (token) {
  return dispatch => {
    dispatch(reqPlanBoard())
    const authToken = `Bearer ${token}`
    const url = `http://tripcomposer.net:12345/rest/planning-board`;
    const myInit = {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        'authorization': authToken,
      }
    }
    return fetch(url, myInit)
              .then(response => response.json())
              .then(data => dispatch(receivePlanBoard(data)))
  }
}


export function addListToBoard (token) {
  return dispatch => {
    dispatch(addCitiesListReq())
    const data = JSON.stringify([
      {
        cityName: "Kyiv",
        countryName: "Ukraine",
        numberOfNights: 0,
        numberOfPersons: 1,
        checkOutDateTime: 1473379200000
      },
      {
        cityName: "Rome",
        countryName: "Italy",
        numberOfNights: 4,
        numberOfPersons: 1,
        checkInDateTime: 1473379200000
      }
    ])
    const authToken = `Bearer ${token}`
    const url = `http://tripcomposer.net:12345/rest/placeToVisit/list`;
    const myInit = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        'authorization': authToken,
      },
      body: data
    }
    return fetch(url, myInit)
              .then(response => response.json())
              .then(data => dispatch(addCitiesListRes(data)))
  }
}

export function addCityToBoard(token, newData, country) {
  return dispatch => {
    dispatch(addCityReq())

    let time = new Date().getTime()
    let compareTime = new Date().setHours(3,0,0,0)
    let departureDate = Date.parse(newData.departureDate) == compareTime ? new Date(newData.departureDate).setHours(23,59,59,99) : Date.parse(newData.departureDate)
    let entranceDate = Date.parse(newData.entranceDate) == compareTime ? new Date(newData.entranceDate).setHours(23,59,59,99) : Date.parse(newData.entranceDate)

    const data = JSON.stringify(
      {
        cityName: newData.cityName,
        countryName: country,
        numberOfNights: parseInt(newData.numberOfNights),
        numberOfPersons: parseInt(newData.numberOfPeople),
        checkOutDateTime: departureDate,
        checkInDateTime: entranceDate
      }
    )
    const authToken = `Bearer ${token}`
    const url = `http://tripcomposer.net:12345/rest/placeToVisit`;
    const myInit = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        'authorization': authToken,
      },
      body: data
    }
    return fetch(url, myInit)
              .then(response => response.json())
              .then(data => dispatch(addCityRes(data)))
  }
}

export function fetchBoardWithCity(token, newData) {
  return dispatch => {
    return fetch(`http://tripcomposer.net:12345/find/city?city=${newData.cityName}`)
              .then(response => response.json())
              .then(data => data[0].countryName)
              .then(country => dispatch(addCityToBoard(token, newData, country)))
  }
}

export function fetchInitTrip (token, tripCities, tripData) {
  return dispatch => {
    dispatch(initTripReq())

    let time = new Date().getTime()
    let compareTime = new Date().setHours(3,0,0,0)
    let startDate = Date.parse(tripData.startDate) == compareTime ? new Date(tripData.startDate).setHours(23,59,59,99) : Date.parse(tripData.startDate)
    let endDate = Date.parse(tripData.finishDate) == compareTime ? new Date(tripData.finishDate).setHours(23,59,59,99) : Date.parse(tripData.finishDate)

    const data = JSON.stringify(
      {
        accommodationComfort: tripData.accomodationType,
        cities: tripCities,
        kidsAges: [
          "3",
          "14"
        ],
        people: tripData.numberOfPersons,
        transportationClass: tripData.transportType,
        tripEndDate: startDate,
        tripStartDate: endDate
      }
    )

    const authToken = `Bearer ${token}`
    const url = `http://tripcomposer.net:12345/rest/initTrip`;
    const myInit = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        'authorization': authToken,
      },
      body: data
    }

    return fetch(url, myInit)
              .then(response => response.json())
              .then(data => dispatch(initTripRes(data)))

  }
}

export function fetchCountriesList (citiesArr) {
  return dispatch => {
    dispatch(reqCountries())
    let data = citiesArr.map((city) =>
       fetch(`http://tripcomposer.net:12345/find/city?city=${city}`)
           .then(response => response.json())
           .then(function(arr){ for(let i = 0; i<arr.length; i++) {
             delete arr[i].apiError
             delete arr[i].success
             delete arr[i].apiProvider
             return arr[i]
           }})
    )
    Promise.all(data).then(value => dispatch(receiveAllCountries(value)))
  }
}
