//main
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute, browserHistory, hashHistory } from 'react-router'
import {configureStore, store} from './store'
import 'babel-polyfill'

//import components(dumb components*(*views))
import Search from './components/Search'

//import containers(smart components)
import Home from './containers/Home'
// import PlanningBoard from './containers/PlanningBoard'



//import styles
import css from './styles/default.styl'

const routes = {
  path: '/',
  component: Home,
  indexRoute: {component: Search},
  childRoutes: [
    {path: '/planboard', component: PlanningBoard}
  ]
}
const main = (
  <Provider store={store}>
    <Router history={browserHistory} routes={routes} />
  </Provider>
)

const startNode = document.getElementById('root')

render(main, startNode);
