import { ADD_TRIP_CITIES, ADD_TRIP_DATES, ADD_TRIP_TRANSPORT_WITH_ACCOMODATION } from '../actions/actionCreators'

function tripFirst (state = {
    isFetching: false,
    cities: {},
    transport: {},
    dates: {}
}, action) {
    switch (action.type) {
    case ADD_TRIP_CITIES:
      return Object.assign({}, state, {
        cities: action.cities
      })
    case ADD_TRIP_DATES:
      return Object.assign({}, state, {
        dates: action.dates
      })
    case ADD_TRIP_TRANSPORT_WITH_ACCOMODATION:
      return Object.assign({}, state, {
        transport: action.transportWithAcc
      })
      default:
        return state
    }
}

export default tripFirst;
