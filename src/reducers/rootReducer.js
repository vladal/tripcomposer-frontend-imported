import { combineReducers } from 'redux'
import user from './user'
import countries from './countriesList'
import planBoard from './planBoard'
import tripFirst from './tripFirst'

const rootReducer = combineReducers({user, tripFirst, countries, planBoard})

export default rootReducer;
