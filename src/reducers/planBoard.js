import { REQ_BOARD, RECEIVE_BOARD, ADD_CITIES, CITIES_ADDED, CITY_ADDED, ADD_CITY, INIT_TRIP_FROM_HP, RECEIVE_TRIP_FROM_HP } from '../actions/actionCreators'

function planBoard (state = {
    isFetching: false,
    data: []
}, action) {
    switch (action.type) {
    case REQ_BOARD:
      return Object.assign({}, state, {
        isFetching: true
      })
    case INIT_TRIP_FROM_HP:
      return Object.assign({}, state, {
        isFetching: true
      })
    case RECEIVE_TRIP_FROM_HP:
      return Object.assign({}, state, {
        isFetching: false,
        data: action.data
      })
    case RECEIVE_BOARD:
      return Object.assign({}, state, {
        isFetching: false,
        data: action.data
      })
    case ADD_CITIES:
      return Object.assign({}, state, {
        isFetching: true
      })
    case CITIES_ADDED:
      return Object.assign({}, state, {
        isFetching: false,
        data: action.data
      })
    case ADD_CITY:
      return Object.assign({}, state, {
        isFetching: true
      })
    case CITY_ADDED:
      return Object.assign({}, state, {
        isFetching: false,
        data: action.data
      })
      default:
        return state
    }
}

export default planBoard;
