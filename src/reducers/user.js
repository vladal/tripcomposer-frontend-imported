import { REQ_TOKEN, RECEIVE_TOKEN, STORED_TOKEN } from '../actions/actionCreators'

function user (state = {
    isFetching: false,
    data: null,
    token: ''
}, action) {
    switch (action.type) {
    case REQ_TOKEN:
      return Object.assign({}, state, {
        isFetching: true
      })
    case RECEIVE_TOKEN:
      return Object.assign({}, state, {
        isFetching: false,
        data: action.data,
        token: action.token
      })
    case STORED_TOKEN:
      return Object.assign({}, state, {
        isFetching: false,
        token: action.token
      })
      default:
        return state
    }
}

export default user;
