import { REQ_COUNTRIES, RECEIVE_COUNTRIES, RECEIVE_CITIES, ADD_CARD, RECEIVE_ALL_COUNTRIES } from '../actions/actionCreators'

function countries (state = {
    isFetching: false,
    data: [],
    startCity: '',
    otherCities: [],
    cities: [],
    allData: []
}, action) {
    switch (action.type) {
    case REQ_COUNTRIES:
      return Object.assign({}, state, {
        isFetching: true
      })
    case ADD_CARD:
      return Object.assign({}, state, {
        isFetching: true,
        otherCities: [...state.otherCities, action.data]
      })
    case RECEIVE_COUNTRIES:
      return Object.assign({}, state, {
        isFetching: false,
        data: action.data
      })
    case RECEIVE_ALL_COUNTRIES:
      return Object.assign({}, state, {
        isFetching: false,
        allData: Array.prototype.concat(action.allData)
      })
    case RECEIVE_CITIES:
      return Object.assign({}, state, {
        isFetching: false,
        startCity: action.startCity,
        otherCities: action.otherCities
      })
      default:
        return state
    }
}

export default countries;
