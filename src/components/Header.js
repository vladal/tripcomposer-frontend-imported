import React, {Component} from 'react';
import { render } from 'react-dom';

class Header extends Component {
  constructor(props){
    super(props)
  }

  render () {
    return(
      <header>
        <div id="logo" className="logo"></div>
        <nav id="navigation" className="nav">
          <a href="#" className="nav-item">Popular trips</a>
          <a href="#" className="nav-item">Transportaion</a>
          <a href="#" className="nav-item">Accomodation</a>
          <a href="#" className="nav-item">About</a>
          <a href="#" className="nav-item">Help</a>
          <a href="#" className="nav-item">Log in</a>
        </nav>
      </header>
    )
  }

}

export default Header;
