import React from 'react';
import { render } from 'react-dom';
import { browserHistory } from 'react-router'
import { Link } from 'react-router';


class Search extends React.Component {
  constructor(props) {
    super(props)

    this.getCities = this.getCities.bind(this)
    this.getDates = this.getDates.bind(this)

    this.getBackToCities = this.getBackToCities.bind(this)
    this.getBackToDates = this.getBackToDates.bind(this)

    this.handleAcValue = this.handleAcValue.bind(this)
    this.handleTrValue = this.handleTrValue.bind(this)

    this.getTransportAndAccomodation = this.getTransportAndAccomodation.bind(this)

    this.getDepartureSuggestions = this.getDepartureSuggestions.bind(this)
    this.setDepartureValue = this.setDepartureValue.bind(this)

    this.getFinishSuggestions = this.getFinishSuggestions.bind(this)
    this.setFinishValue = this.setFinishValue.bind(this)

    this.toggleSuggestDep = this.toggleSuggestDep.bind(this)
    this.toggleSuggestFin = this.toggleSuggestFin.bind(this)
    this.toggleChildsForm = this.toggleChildsForm.bind(this)

    this.childsArray = this.childsArray.bind(this)



    this.state = {
                  cities: true,
                  dates: false,
                  transport: false,
                  acValue: "Accomodation comfort",
                  trValue: "Transportaion",
                  departureValue: '',
                  finisValue: '',
                  finishAC: false,
                  departureAC: false,
                  childsShowed: false,
                  childsArr: [],
                  citiesWithCountires: [],
                  initTripData: {}
                 }
  }

  getBackToCities (e) {
    e.preventDefault()
    this.setState({
      cities: true,
      dates: false,
      transport: false
    })
    return false

  }
  getBackToDates (e) {
    e.preventDefault()
    this.setState({
      cities: false,
      dates: true,
      transport: false
    })
    return false

  }
  getCities(e) {
    e.preventDefault()
    let form = e.target
    let start = form[0].value
    let other = form[1].value.split(',') != '' ? form[1].value.split(',') : []
    let arr = other
    other.unshift(start)
    this.props.fetchCountriesList(other)
    this.setState({
      cities: false,
      dates: true,
      transport: false,
      citiesWithCountires: this.props.countries.allData
    })
    console.log(this.state.citiesWithCountires)
  };
  handleAcValue (e) {
    this.setState({
      acValue: e.target.value
    })
  }
  handleTrValue (e) {
    this.setState({
      trValue: e.target.value
    })
  }
  getTransportAndAccomodation (e) {
    e.preventDefault()
    let token = this.props.user.token || localStorage.auth_token
    var someData;
    let arr = e.target
    for (let i = 0, data={}; i < arr.length - 1; i++) {
      if (arr[i].id !== undefined) {
        data[arr[i].id] = arr[i].value
      }
      someData = data
    }
    console.log(someData)
    this.props.addTripTransportWithAcc(someData)
    this.setState({initTripData: Object.assign(this.state.initTripData, someData)})
    console.log(this.state.initTripData)
    this.props.fetchInitTrip(token, this.props.countries.allData, this.state.initTripData)

  }

  getDepartureSuggestions (e) {
    let value = e.target.value
    this.props.citiesToCountries(value)
    this.setState({
      departureValue: value
    })
  }
  setDepartureValue (e, value) {
    value = e.target.innerText
    this.props.citiesToCountries(value)
    this.setState({
      departureValue: value
    })
  }
  getFinishSuggestions (e) {
    let len = this.state.finisValue.length

    let value = e.target.value
    let usedValue = this.state.finisValue.indexOf(',') >= 1 ? e.target.value.slice(this.state.finisValue.lastIndexOf(',') + 1) : e.target.value
    this.props.citiesToCountries(usedValue)
    this.setState({
      finisValue: value
    })
  }
  childsArray(e, num, some) {
    some = ''
    var arr = []
    num = e.target.value
    for (let i=0; i<num; i++) {
      arr.push(some)
    }
    this.setState({childsArr: arr})
    console.log(this.state.childsArr)
  }
  setFinishValue (e, value) {
    let commaLen = this.state.finisValue.lastIndexOf(',') + 1
    let someString = this.state.finisValue.substring(0, commaLen)
    value = someString + e.target.innerText
    this.setState({
      finisValue: value
    })
  }
  toggleSuggestDep(){
    if (!this.state.departureAC) {
      this.setState({departureAC:true})
    } else{
      this.setState({departureAC:false})
    }
  }
  toggleSuggestFin(){
    if (!this.state.finishAC) {
      this.setState({finishAC:true})
    } else{
      this.setState({finishAC:false})
    }
  }
  toggleChildsForm () {
      this.setState({childsShowed:true})

    // if (!this.state.childsShowed) {
    //   this.setState({childsShowed:true})
    // } else{
    //   this.setState({childsShowed:false})
    // }
  }



  getDates(e) {
    e.preventDefault()
    var newData;
    let arr = e.target
    for (let i = 0, data={}; i < arr.length - 1; i++) {
      if (arr[i].id !== undefined) {
        data[arr[i].id] = arr[i].value
      }
      newData = data
    }
    console.log(newData)
    this.props.addTripDates(newData)
    this.setState({initTripData: Object.assign(this.state.initTripData, newData)})
    this.setState({
      cities: false,
      dates: false,
      transport: true
    })
    return false
  };

  render() {
    const suggest = this.props.countries.data
    const departureAC = this.state.departureAC
    const finishAC = this.state.finishAC
    const childsShowed = this.state.childsShowed
    const childsCount = this.state.childsArr

    return(
      <section id="search" >
        <div className="search-wrapper">
          <h1>Trip Composer</h1>
          <h2>Plan your multi-city trip with transportation and accomodation in one click</h2>
          {this.state.cities === true ?
            <form className="search" onSubmit={this.getCities}>
              <input type="text"
                     className="from-point"
                     value={this.state.departureValue}
                     placeholder="Departure city"
                     onChange={this.getDepartureSuggestions}
                     onFocus={this.toggleSuggestDep}
                     onBlur={this.toggleSuggestDep}
                     required/>
              {departureAC === true && suggest.length > 0 ?
                <div className="departure-autocomplete">

                  {suggest.map((city, i) =>
                    <p className="departure-suggest"
                       key={i}
                       onMouseDown={this.setDepartureValue}>{city.cityName}</p>
                     )}

                </div>
               : null}


              <input type="text"
                     className="to-point"
                     value={this.state.finisValue}
                     placeholder="Where do you want to go?"
                     onChange={this.getFinishSuggestions}
                     onFocus={this.toggleSuggestFin}
                     onBlur={this.toggleSuggestFin}/>
              {finishAC === true && suggest.length > 0 ?
                <div className="finish-autocomplete">

                  {suggest.map((city, i) =>
                    <p className="finish-suggest"
                       key={i}
                       onMouseDown={this.setFinishValue}>{city.cityName}</p>
                   )}

                </div>
              : null}


              <button className="submit-btn next-btn btn" >Next</button>
            </form>
            : null
          }
          {this.state.dates === true ?
            <form className="search" onSubmit={this.getDates}>
              <div className="submit-btn back-btn btn div-btn" onClick={this.getBackToCities}>Back</div>
              <input type="date" id="startDate" className="start-date" required/>
              <input type="date" id="finishDate" className="finish-date" required/>
              <input type="number" id="numberOfPersons" className="persons" placeholder="Adults"/>
              <span className="childrens" onClick={this.toggleChildsForm}>Childs
                {childsShowed === true ?
                  <div className="childs-form">
                    <label htmlFor="numberOfChilds" className="child-input-label">Children</label>
                    <input type="number" id="numberOfChilds" max="5"  className="child-input" onInput={this.childsArray}/>
                    {childsCount.map((item, i) =>
                      <span key={i+16}>
                        <label key={i} className="child-input-label" htmlFor={`child_${i}`}>{`Child ${i+1} age`}</label>
                        <input key={i+1} type="number" id={`child_${i}`} className="child-input"/>
                      </span>
                    )}
                  </div>
                : null }
              </span>
              <button className="submit-btn next-btn btn" >Next</button>
            </form>
            : null
          }
          {this.state.transport === true ?
            <form className="search" onSubmit={this.getTransportAndAccomodation}>
              <div className="submit-btn back-btn btn div-btn" onClick={this.getBackToDates}>Back</div>
              <select className="transport-type" name="transportation" id="transportType" value={this.state.trValue} onChange={this.handleTrValue}>
                <option value="Transportaion" disabled>Transportaion</option>
                <option value="Cheapest">Cheapest</option>
                <option value="Economy">Economy</option>
                <option value="Business">Business</option>
                <option value="Premium">Premium</option>
              </select>
              <select className="accomodation-type" name="accomodation" id="accomodationType" value={this.state.acValue} onChange={this.handleAcValue}>
                <option value="Accomodation comfort" disabled>Accomodation comfort</option>
                <option value="Dorm room">Dorm room</option>
                <option value="Hostel private room">Hostel private room</option>
                <option value="Hotel 3-stars and better">Hotel 3-stars and better</option>
                <option value="Hotel 4-stars and better">Hotel 4-stars and better</option>
                <option value="Hotel 5-stars">Hotel 5-stars</option>
              </select>
              <button className="submit-btn next-btn btn" >Plan</button>
            </form>
            : null
          }

        </div>
      </section>
    )
  }
}

export default Search;
