import React from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionCreators from '../actions/actionCreators'
import Header from '../components/Header'

class Start extends React.Component {
  constructor(props) {
    super(props);
    this.props.logIn()
  }

  render() {
    return(
      <main>
        <Header />
        {React.cloneElement(this.props.children, this.props)}
      </main>
    )
  }

}

function mapStateToProps (state) {
  return {
    user: state.user,
    countries: state.countries,
    tripFirst: state.tripFirst
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(actionCreators, dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(Start);
